<?php

/**
 * @file
 *   Defines an IFrame provider for Scald.
 */

define('IFRAME_DEFAULT_WIDTH', 940);
define('IFRAME_DEFAULT_HEIGHT', 365);

/**
 * Implements hook_scald_atom_providers().
 * Tell Scald that we'll be providing some iframe atoms.
 */
function scald_iframe_scald_atom_providers() {
  return array(
    'iframe' => 'Iframe'
  );
}

/**
 * Implements hook_scald_atom_providers_opt().
 * Tell Scald that we don't need the 'add' step of the ctools wizard.
 */
function scald_iframe_scald_atom_providers_opt() {
  return array(
    'iframe' => array(
      'starting_step' => 'options',
    ),
  );
}

/**
 * Implements hook_scald_fetch().
 */
function scald_iframe_scald_fetch($atom, $type) {
  if ($type != 'atom') {
    return;
  }

  // Iframe thumbnail path
  $thumbnail = field_get_items('scald_atom', $atom, 'scald_thumbnail');
  if (!empty($thumbnail)) {
    $file = $thumbnail[0]['uri'];
    if (file_exists($file)) {
      $atom->file_source = $atom->thumbnail_source = $file;
    }
  }

  // Iframe URL
  $url = field_get_items('scald_atom', $atom, 'scald_url');
  if (!empty($url)) {
    $atom->iframe_url = $url[0]['value'];
  }

  // Iframe width
  $width = field_get_items('scald_atom', $atom, 'scald_width');
  if (!empty($width)) {
    $atom->iframe_width = $width[0]['value'];
  }

  // Iframe height
  $height = field_get_items('scald_atom', $atom, 'scald_height');
  if (!empty($height)) {
    $atom->iframe_height = $height[0]['value'];
  }
}

/**
 * Implements hook_scald_prerender().
 */
function scald_iframe_scald_prerender($atom, $context, $options, $mode) {
  if ($mode != 'atom') {
    return;
  }

  $width = !empty($atom->iframe_width) ? $atom->iframe_width : IFRAME_DEFAULT_WIDTH;
  $height = !empty($atom->iframe_height) ? $atom->iframe_height : IFRAME_DEFAULT_HEIGHT;
  $url = !empty($atom->iframe_url) ? $atom->iframe_url : '';
  $thumbnail = !empty($atom->thumbnail_source) ? $atom->thumbnail_source : '';

  $ratio = $height / $width * 100;
  $config = scald_context_config_load($context);
  $player = $config->player[$atom->type];
  $theme = 'scald_iframe_player';

  // Switch player if necessary.
  if (isset($player['*']) && $player['*'] == 'iframe_inbox') {
    $theme = 'scald_iframe_inbox';
  }

  // Render the player.
  $atom->rendered->player = theme($theme,
    array(
      'vars' =>
        array(
          'iframe_url' => $url,
          'iframe_width' => $width,
          'iframe_height' => $height,
          'iframe_ratio' => $ratio . '%',
          'thumbnail' => $thumbnail,
        ),
      'atom' => $atom,
    )
  );
}

/**
 * Implements hook_scald_player().
 */
function scald_iframe_scald_player() {
  return array(
    'iframe_inbox' => array(
      'name' => 'Inbox Iframe',
      'description' => 'Display an image (thumbnail) and allow to open the iframe in a box (colorbox, magnific_popup, ...)',
      'type' => array('iframe')
    ),
  );
}

/**
 * Implements hook_theme().
 */
function scald_iframe_theme() {
  return array(
    'scald_iframe_player' => array(
      'variables' => array('vars' => NULL, 'atom' => NULL),
      'template' => 'scald-iframe-player'
    ),
    'scald_iframe_inbox' => array(
      'variables' => array('vars' => NULL, 'atom' => NULL),
      'template' => 'scald-iframe-inbox'
    )
  );
}
