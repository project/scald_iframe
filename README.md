Scald Iframe
============

The Scald Iframe module adds a new scald provider for adding iframe atoms.

Scald Iframe players
--------------------

There are two ways of displaying an iframe atom. 
The first one is to embed the iframe directly in the page with specified width and height.
The second one is to add the specified thumbnail to the page and a link to open the iframe into a modal box.